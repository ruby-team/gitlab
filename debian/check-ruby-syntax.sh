# For whole tree
find . -name \*.rb -exec ruby -wc {} \; 2>&1 | grep error
# For changed files
for i in `git diff --name-only debian/13.1.1-1 HEAD | grep \.rb\$`; do ruby -wc $i; done
