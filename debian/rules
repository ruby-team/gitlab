#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk
BUILDDIR := _build/src/gitlab.com/gitlab-org/gitlab
VENDORDIR := ${BUILDDIR}/vendor
SYSTEMGOCODE := /usr/share/gocode/src

%:
	dh $@ --buildsystem=ruby --with=ruby --package=gitlab
	dh $@ --buildsystem=golang --with=golang --builddirectory=_build \
	--package=gitlab-workhorse --sourcedirectory=workhorse

override_dh_auto_configure-indep:
	dh_auto_configure -O--package=gitlab -O--buildsystem=ruby -O--with=ruby

override_dh_auto_configure-arch:
	dh_auto_configure -O--package=gitlab-workhorse -O--buildsystem=golang \
	-O--with=golang -O--builddirectory=_build -O--sourcedirectory=workhorse
	# we don't need gitlab's (ruby) vendor directory
	rm -rf ${VENDORDIR}
	# workhorse-vendor is meant for workhorse, but should be renamed
	mv ${BUILDDIR}/workhorse-vendor ${VENDORDIR}
	# merge vendor from gitaly with vendor in build directory using simbolic links
	# if the command fails with errors like cp: not replacing '_build/src/gitlab.com/gitlab-org/gitlab/vendor/...'
	# then remove any conflicting modules from workhorse-vendor tarball
	cp -arsn ${SYSTEMGOCODE}/gitlab.com/gitlab-org/gitaly/v16/vendor/* \
	${VENDORDIR}/
	# import paths in vendor needs version as directory (go.mod is ignored)
	mkdir -p ${VENDORDIR}/github.com/cespare/xxhash/v2
	mkdir -p ${VENDORDIR}/github.com/oklog/ulid/v2
	cp -arsn ${SYSTEMGOCODE}/github.com/cespare/xxhash/* ${VENDORDIR}/github.com/cespare/xxhash/v2
	cp -arsn ${SYSTEMGOCODE}/github.com/oklog/ulid/* ${VENDORDIR}/github.com/oklog/ulid/v2
	# only go files gets copied, but we need files like gitlab-logo.png
	cp -rf workhorse ${BUILDDIR}
	# Remove non-existent symlink
	find ${BUILDDIR} -name test.git -delete

override_dh_auto_build-indep:
	dh_auto_build -O--package=gitlab -O--buildsystem=ruby -O--with=ruby

override_dh_auto_build-arch:
	dh_auto_build -O--package=gitlab-workhorse -O--buildsystem=golang \
	-O--with=golang -O--builddirectory=_build -O--sourcedirectory=workhorse
	# Rename binary to gitlab-workhorse (upstream is passing -o in Makefile)
	if [ -f _build/bin/workhorse ]; then \
	mv _build/bin/workhorse _build/bin/gitlab-workhorse; fi

override_dh_auto_test-indep:
	dh_auto_test -O--package=gitlab -O--buildsystem=ruby -O--with=ruby

override_dh_auto_test-arch:
	PATH="$(CURDIR)/_build/bin:$$PATH" dh_auto_test -O--package=gitlab-workhorse \
	-O--buildsystem=golang -O--with=golang -O--builddirectory=_build \
	-O--sourcedirectory=workhorse

override_dh_auto_install-indep:
	# skip gem2deb copying files to /usr/lib/ruby

override_dh_auto_install-arch:
	dh_auto_install -O--package=gitlab-workhorse -O--buildsystem=golang \
	-O--with=golang -O--builddirectory=_build -O--sourcedirectory=workhorse

override_dh_install-indep:
	sh debian/upstream-file-count-check.sh
	sh debian/upstream-config-file-check.sh
	dh_install -XLICENSE -O--package=gitlab -O--buildsystem=ruby -O--with=ruby
	dh_installexamples -O--package=gitlab -O--buildsystem=ruby -O--with=ruby
	# Make sure we are installing all required files in debian/install
	rm -rf debian/gitlab/usr/share/gitlab/tmp/*
	find debian/gitlab/usr/share/gitlab/ -name .eslintrc.yml -delete
	mv debian/gitlab/usr/share/gitlab/app/assets/javascripts/locale \
	debian/gitlab/usr/share/gitlab/app/assets/javascripts/locale.static
	if [ -f debian/gitlab/var/lib/gitlab/db/structure.sql ]; then \
	  mv debian/gitlab/var/lib/gitlab/db/structure.sql debian/gitlab/var/lib/gitlab/db/structure.sql.template; fi
	sed -i 's/__NEW_VERSION__/${DEB_VERSION}/g' debian/gitlab/usr/lib/gitlab/templates/gitlab-debian.conf.example

override_dh_install-arch:
	dh_install -XLICENSE -O--package=gitlab-workhorse -O--buildsystem=golang \
	-O--with=golang -O--builddirectory=_build -O--sourcedirectory=workhorse

execute_before_dh_installdeb:
	if command -v dh_movetousr >/dev/null; then dh_movetousr; fi

override_dh_installsystemd-indep:
	dh_installsystemd --no-start -p gitlab --name=gitlab-sidekiq -O--package=gitlab
	dh_installsystemd --no-start -p gitlab --name=gitlab-puma -O--package=gitlab
	dh_installsystemd --no-start -p gitlab --name=gitlab-mailroom -O--package=gitlab
	dh_installsystemd --no-start -p gitlab --name=gitlab-workhorse -O--package=gitlab
	dh_installsystemd -O--package=gitlab -O--buildsystem=ruby -O--with=ruby

override_dh_golang-indep:
	# don't fail in arch:all build, is there a better way ?

override_dh_dwz:
	# Do nothing
